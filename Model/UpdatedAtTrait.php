<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
trait UpdatedAtTrait
{
    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}