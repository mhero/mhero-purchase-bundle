<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
interface PurchaseUserInterface
{
	/**
	 * @return DateTime
	 */
	function getLastPurchaseDate();
	
	/**
	 * @param DateTime $lastPurchaseDate
	 */
	function setLastPurchaseDate(DateTime $lastPurchaseDate);
}