<?php

namespace PurchaseBundle\Model;

/**
 * Interface CreationDate
 *
 * @author David Wolter <david@lovoo.com>
 */
interface CreationDateInterface
{
    /**
     * @return \DateTime
     */
    public function getCreationDate();

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate = null);

    /**
     * @return string
     */
    public function getCreationDay();
}
