<?php

namespace PurchaseBundle\Model;

/**
 * Interface Id
 *
 * @author David Wolter <david@lovoo.com>
 */
interface IdInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @param string $id
     */
    public function setId($id);
}
