<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
interface PurchaseLogInterface extends IdInterface, CreationDateInterface, UpdatedAtInterface
{
    /**
     * Returns the new state.
     *
     * @return string
     */
    function getNewState();

    /**
     * Sets the new state.
     *
     * @param string $newState
     */
    function setNewState($newState);

    /**
     * Returns the old state.
     *
     * @return string
     */
    function getOldState();

    /**
     * Sets the old state.
     *
     * @param string $oldState
     */
    function setOldState($oldState);

    /**
     * Returns the extra data.
     *
     * @return string
     */
    function getExtraData();

    /**
     * Sets the extra data.
     *
     * @param string $extraData
     */
    function setExtraData($extraData);

    /**
     * Returns the purchase.
     *
     * @return PurchaseInterface
     */
    function getPurchase();

    /**
     * Sets the advert.
     *
     * @param PurchaseInterface $purchase
     */
    function setPurchase(PurchaseInterface $purchase);
}
