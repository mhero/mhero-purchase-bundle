<?php

namespace PurchaseBundle\Model;

/**
 * Trait UserAware
 *
 * @author David Wolter <david@lovoo.com>
 */
trait UserAwareTrait
{
    /**
     * @var PurchaseUserInterface
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser($user = null)
    {
        $this->user = $user;
    }
}
