<?php

namespace PurchaseBundle\Model;

/**
 * Trait Type
 *
 * @author David Wolter <david@lovoo.com>
 */
trait TypeTrait
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType($type = null)
    {
        $this->type = $type;
    }
}
