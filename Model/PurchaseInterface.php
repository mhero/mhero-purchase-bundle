<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
interface PurchaseInterface extends IdInterface, CreationDateInterface, UpdatedAtInterface, TypeInterface, UserAwareInterface
{
    /**
     * Returns the reference.
     *
     * @return string
     */
    function getReference();

    /**
     * Sets the reference.
     *
     * @param string $reference
     */
    function setReference($reference);

    /**
     * Returns the state.
     *
     * @return string
     */
    function getState();

    /**
     * Sets the state.
     *
     * @param string $state
     */
    function setState($state);
	
	/**
	 * Returns the package name.
	 *
	 * @return string
	 */
	function getPackageName();
	
	/**
	 * Sets the package name.
	 *
	 * @param string $packageName
	 */
	function setPackageName($packageName);
	
	/**
	 * Returns the product.
	 *
	 * @return string
	 */
	function getProduct();
	
	/**
	 * Sets the product.
	 *
	 * @param string $product
	 */
	function setProduct($product);
	
	/**
	 * Returns the purchase token.
	 *
	 * @return string
	 */
	function getToken();
	
	/**
	 * Sets the purchase token.
	 *
	 * @param string $token
	 */
	function setToken($token);
	
	/**
	 * Returns the purchase state as it is from provider side.
	 *
	 * @return string
	 */
	function getPurchaseState();
	
	/**
	 * Sets the purchase state that is sent by the provider.
	 *
	 * @param string $state
	 */
	function setPurchaseState($state);
	
	/**
	 * Returns the original json data.
	 *
	 * @return array
	 */
	function getOriginalJson();
	
	/**
	 * Sets the original json data.
	 *
	 * @param array $json
	 */
	function setOriginalJson(array $json);
	
	/**
	 * Returns the additional data.
	 *
	 * @return array
	 */
	function getAdditionalData();
	
	/**
	 * Sets the additional data.
	 *
	 * @param array $additionalData
	 */
	function setAdditionalData(array $additionalData);
	
	/**
	 * Gets if the transaction is verified or not.
	 *
	 * @return bool
	 */
	function getVerify();
	
	/**
	 * Sets the verify value, default is false.
	 *
	 * @param bool $verify [default = false]
	 */
	function setVerify($verify = false);
}
