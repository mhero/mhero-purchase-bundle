<?php

namespace PurchaseBundle\Model;

/**
 * Interface UserAware
 *
 * @author David Wolter <david@lovoo.com>
 */
interface UserAwareInterface
{
    /**
     * @return PurchaseUserInterface
     */
    public function getUser();

    /**
     * @param PurchaseUserInterface|null $user
     */
    public function setUser($user = null);
}
