<?php

namespace PurchaseBundle\Model;

/**
 * Trait Id
 *
 * @author David Wolter <david@lovoo.com>
 */
trait IdTrait
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
