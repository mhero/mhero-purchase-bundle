<?php
namespace PurchaseBundle\Model;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class Purchase implements PurchaseInterface
{
    use IdTrait, CreationDateTrait, UpdatedAtTrait, TypeTrait, UserAwareTrait;

    /**
     * @var string
     */
    protected $reference;
    /**
     * @var string
     */
    protected $state;
	/**
	 * @var string
	 */
	protected $packageName;
	/**
	 * @var string
	 */
	protected $product;
	/**
	 * @var string
	 */
	protected $token;
	/**
	 * @var string
	 */
	protected $purchaseState;
	/**
	 * @var array
	 */
	protected $originalJson = [];
	/**
	 * @var array
	 */
	protected $additionalData = [];
	/**
	 * @var bool
	 */
	protected $verify;



    /**
     * {@inheritDoc}
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * {@inheritDoc}
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * {@inheritDoc}
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * {@inheritDoc}
     */
    public function setState($state)
    {
        $this->state = $state;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public function getPackageName()
	{
		return $this->packageName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setPackageName($packageName)
	{
		$this->packageName = $packageName;
	}
	
	/**
	 * {@inheritDoc}
	 */
	function getProduct()
	{
		return $this->product;
	}
	
	/**
	 * {@inheritDoc}
	 */
	function setProduct($product)
	{
		$this->product = $product;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getToken()
	{
		return $this->token;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setToken($token)
	{
		$this->token = $token;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getPurchaseState()
	{
		return $this->purchaseState;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setPurchaseState($state)
	{
		$this->purchaseState = $state;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOriginalJson()
	{
		return $this->originalJson;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setOriginalJson(array $json)
	{
		$this->originalJson = $json;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getAdditionalData()
	{
		return $this->additionalData;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setAdditionalData(array $additionalData)
	{
		$this->additionalData = $additionalData;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getVerify()
	{
		return $this->verify;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setVerify($verify = false)
	{
		$this->verify = $verify;
	}
}
