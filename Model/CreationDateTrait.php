<?php

namespace PurchaseBundle\Model;

/**
 * Trait CreationTime
 *
 * @author David Wolter <david@lovoo.com>
 */
trait CreationDateTrait
{
    /**
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * @var string
     */
    protected $creationDay;

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate = null)
    {
        $this->creationDate = $creationDate;
        $this->creationDay = null !== $creationDate ? $creationDate->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getCreationDay()
    {
        return $this->creationDay;
    }
}
