<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class PurchaseLog implements PurchaseLogInterface
{
    use IdTrait, CreationDateTrait, UpdatedAtTrait;

    /**
     * @var string
     */
    protected $newState;
    /**
     * @var string
     */
    protected $oldState;
    /**
     * @var string
     */
    protected $extraData;
    /**
     * @var PurchaseInterface
     */
    protected $purchase;


    /**
     * {@inheritDoc}
     */
    public function getNewState()
    {
        return $this->newState;
    }

    /**
     * {@inheritDoc}
     */
    public function setNewState($newState)
    {
        $this->newState = $newState;
    }

    /**
     * {@inheritDoc}
     */
    public function getOldState()
    {
        return $this->oldState;
    }

    /**
     * {@inheritDoc}
     */
    public function setOldState($oldState)
    {
        $this->oldState = $oldState;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtraData()
    {
        return $this->extraData;
    }

    /**
     * {@inheritDoc}
     */
    public function setExtraData($extraData)
    {
        $this->extraData = $extraData;
    }

    /**
     * {@inheritDoc}
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * {@inheritDoc}
     */
    public function setPurchase(PurchaseInterface $purchase)
    {
        $this->purchase = $purchase;
    }
}
