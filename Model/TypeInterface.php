<?php

namespace PurchaseBundle\Model;

/**
 * Interface Type
 *
 * @author David Wolter <david@lovoo.com>
 */
interface TypeInterface
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @param string|null $type
     */
    public function setType($type = null);
}
