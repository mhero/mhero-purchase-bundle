<?php
namespace PurchaseBundle\Model;

use DateTime;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
interface UpdatedAtInterface
{
    /**
     * @return DateTime
     */
    function getUpdatedAt();

    /**
     * @param DateTime $updatedAt
     */
    function setUpdatedAt(DateTime $updatedAt);
}