<?php

namespace PurchaseBundle\Tests\AppBundle\Controller;

use PurchaseBundle\Tests\AbstractClientTestCase;

class PurchaseControllerTest extends AbstractClientTestCase
{
    public function testVerify()
    {
        $client = static::createClient();

        $this->doSuccessfulRequest('GET', '/purchase/verify', [], $this->model->user->postUser("Udo"));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
