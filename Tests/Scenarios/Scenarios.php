<?php

namespace PurchaseBundle\Tests\Scenarios;

use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class Scenarios
 *
 * @author David Wolter <david@lovoo.com>
 */
class Scenarios
{
    /**
     * @var Api
     */
    public $api;
    /**
     * @var Model
     */
    public $model;
    /**
     * @var ApiHydrater
     */
    public $hydrator;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->api = new Api($client);
        $this->model = new Model($client);
        $this->hydrator = new ApiHydrater($client);
    }
}