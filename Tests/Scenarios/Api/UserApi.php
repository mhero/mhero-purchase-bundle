<?php

namespace PurchaseBundle\Tests\Scenarios\Api;

use Miner\User\Model\UserInterface;
use PurchaseBundle\Tests\Scenarios\ApiHydrater;
use PurchaseBundle\Tests\Scenarios\ApiOptions;

/**
 * Class UserApi
 *
 * @author David Wolter <david@lovoo.com>
 */
class UserApi
{
    /**
     * @param ApiOptions $options
     */
    public static function getUsers(ApiOptions $options)
    {
        $options->setRoute('GET', '/users');
        $options->setHydrator(ApiHydrater::USER);
    }

    /**
     * @param ApiOptions $options
     */
    public static function getUser(ApiOptions $options)
    {
        $options->setRoute('GET', sprintf('/users/%s', self::getUserId($options)));
        $options->setHydrator(ApiHydrater::USER);
    }

    /**
     * @param ApiOptions $options
     */
    public static function getUserLikes(ApiOptions $options)
    {
        $options->setRoute('GET', sprintf('/users/%s/likes', self::getUserId($options, true)));
    }

    /**
     * @param ApiOptions $options
     */
    public static function postUser(ApiOptions $options)
    {
        $options->setRoute('POST', '/users');
        $options->setHydrator(ApiHydrater::USER);

        if ($options->isRandom()) {
            $options->addParameter(
                array(
                    'name'                  => 'user'.mt_rand(100, 999),
                )
            );
        }
    }

    /**
     * @param ApiOptions $options
     */
    public static function putUserDevice(ApiOptions $options)
    {
        $options->setRoute('PUT', sprintf('/users/%s/device', self::getUserId($options)));

        if ($options->isRandom()) {
            $options->addParameter(
                array(
                    'device_type' => 'ios',
                    'push_token'  => md5(time().mt_rand(100000, 999999)),
                )
            );
        }
    }

    /**
     * @param ApiOptions $options
     */
    public static function deleteUser(ApiOptions $options)
    {
        $options->setRoute('DELETE', sprintf('/users/%s', self::getUserId($options)));
    }

    /**
     * @param ApiOptions $options
     * @param bool       $prefereSecondUser
     *
     * @return mixed
     */
    public static function getUserId(ApiOptions $options, $prefereSecondUser = false)
    {
        if (isset($options->parameter['user_id'])) {
            return $options->parameter['user_id'];
        } else {
            /** @var UserInterface[] $users */
            $users = [$options->user, $options->secondUser];
            if ($prefereSecondUser) {
                $users = [$options->secondUser, $options->user];
            }
            foreach ($users as $user) {
                if (null !== $user) {
                    return $user->getId();
                }
            }
        }

        throw new \RuntimeException('user, secondUser or user_id is missing');
    }
}