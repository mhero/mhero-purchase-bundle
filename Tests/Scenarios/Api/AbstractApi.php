<?php

namespace PurchaseBundle\Tests\Scenarios\Api;

use ApiBundle\Enum\ApiVersions;
use ApiBundle\Request\ApiRequest;
use Miner\User\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use PurchaseBundle\Tests\Scenarios\ApiHydrater;
use PurchaseBundle\Tests\Scenarios\ApiOptions;

/**
 * Class AbstractApi
 *
 * @author David Wolter <david@lovoo.com>
 */
abstract class AbstractApi
{
    /**
     * @var Client
     */
    protected $client;
    /**
     * @var ApiHydrater
     */
    public $hydrator;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->hydrator = new ApiHydrater($client);
    }

    /**
     * @param ApiOptions $options
     *
     * @return mixed
     */
    protected function goApi(ApiOptions $options)
    {
        $data = $this->doRequest($options);

        if ($options->mustHydrate()) {
            $data = $this->hydrator->hydrate($options->getHydrator(), $data);
        }

        return $data;
    }

    /**
     * @param ApiOptions $options
     *
     * @return object
     */
    protected function doRequest(ApiOptions $options)
    {
        list($method, $route) = $options->getRoute();

        if ($options->isSuccess()) {
            $data = $this->doSuccessfulRequest(
                $method,
                $route,
                $options->parameter,
                $options->user,
                $options->server
            );
        } else {
            $data = $this->request(
                $method,
                $route,
                $options->parameter,
                $options->user,
                $options->server
            );
        }

        return $data;
    }

    /**
     * @param               $method
     * @param               $uri
     * @param array         $parameters
     * @param UserInterface $user
     * @param array         $server
     *
     * @return object
     */
    protected function doSuccessfulRequest(
        $method,
        $uri,
        array $parameters = array(),
        UserInterface $user = null,
        array $server = array()
    ) {
        $content = $this->request($method, $uri, $parameters, $user, $server);

        self::assertResponseSuccessful();

        return $content;
    }

    /**
     * @param               $method
     * @param               $uri
     * @param array         $parameters
     * @param UserInterface $user
     * @param array         $server
     *
     * @return object
     */
    protected function request(
        $method,
        $uri,
        array $parameters = array(),
        UserInterface $user = null,
        array $server = array()
    ) {
        $server['HTTP_accept'] = 'application/json';

        if (!isset($server['HTTP_'.ApiRequest::HEADER_API_VERSION])) {
            $server['HTTP_'.ApiRequest::HEADER_API_VERSION] = ApiVersions::CURRENT;
        }
        if (!isset($server['HTTP_'.ApiRequest::HEADER_PLATFORM])) {
            $server['HTTP_'.ApiRequest::HEADER_PLATFORM] = ApiRequest::PLATFORM_IOS;
        }

//        if (null !== $user) {
//            $server['HTTP_'.ApiRequest::HEADER_API_KEY] = sprintf(
//                'fb-%s-%s',
//                $user->getFacebook()->getId(),
//                $user->getPassword()
//            );
//        }

        $this->client->request($method, $uri, $parameters, array(), $server);

        return json_decode($this->client->getResponse()->getContent());
    }

    /**
     * @param int $code
     */
    public function assertResponseSuccessful($code = null)
    {
        $codeReturned = $this->client->getResponse()->getStatusCode();
        $codeReturnedString = (string) $codeReturned;

        $message =
            '[Request]'.PHP_EOL.$this->client->getRequest()->getRequestUri().PHP_EOL.
            '[Response]'.PHP_EOL.$this->client->getResponse()->getContent().PHP_EOL;

        if (null === $code) {
            \PHPUnit_Framework_TestCase::assertTrue(2 === (int) $codeReturnedString[0], $message);
        } else {
            \PHPUnit_Framework_TestCase::assertEquals($code, $codeReturned, $message);
        }
    }
}