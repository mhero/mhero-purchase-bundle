<?php

namespace PurchaseBundle\Tests\Scenarios;

use PurchaseBundle\Tests\Scenarios\Api\AbstractApi;
use PurchaseBundle\Tests\Scenarios\Api\UserApi;
use Miner\User\Model\UserInterface;

/**
 * Class Api
 *
 * @author David Wolter <david@lovoo.com>
 */
class Api extends AbstractApi
{
    /**
     * @return UserInterface|object
     */
    public function getUser()
    {
        UserApi::getUser($options = ApiOptions::buildOptions(func_get_args()));

        return $this->goApi($options);
    }

    /**
     * @return UserInterface[]|object
     */
    public function getUsers()
    {
        UserApi::getUsers($options = ApiOptions::buildOptions(func_get_args()));

        return $this->goApi($options);
    }

    /**
     * @return UserInterface|object
     */
    public function postUser()
    {
        UserApi::postUser($options = ApiOptions::buildOptions(func_get_args()));

        return $this->goApi($options);
    }

    /**
     * @return object
     */
    public function putUserDevice()
    {
        UserApi::putUserDevice($options = ApiOptions::buildOptions(func_get_args()));

        return $this->goApi($options);
    }

    /**
     * @return object
     */
    public function deleteUser()
    {
        UserApi::deleteUser($options = ApiOptions::buildOptions(func_get_args()));

        return $this->goApi($options);
    }
}