<?php

namespace PurchaseBundle\Tests\Scenarios\Model;

use AppBundle\Entity\User;
use Miner\User\Model\UserInterface;

/**
 * Class UserModel
 *
 * @author David Wolter <david@lovoo.com>
 */
class UserModel extends AbstractModel
{
    /**
     * @param string $name
     * @param array  $params
     *
     * @return UserInterface
     */
    public function postUser($name = 'user', array $params = array())
    {
        $user = new User();
        $user->setUsername($name);
        $user->setPassword('password');

        foreach($params as $key => $value) {
            if ($key == "device") {
                foreach ($value as $deviceKey => $deviceValue) {
                    $method = 'set' . ucfirst($deviceKey);
                    $user->getDevice()->$method($deviceValue);
                }
            } else {
                $method = 'set' . ucfirst($key);
                $user->$method($value);
            }
        }

        $this->saveObject($user);

        return $user;
    }
}