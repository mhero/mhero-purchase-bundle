<?php
namespace PurchaseBundle\Tests\Scenarios\Model;

use DateTime;
use PurchaseBundle\Entity\Purchase;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseModel extends AbstractModel
{
	public function postPurchase()
	{
		$purchase = new Purchase();
		
		/**
		 * todo: add purchase mock
		 + $this->saveObject($purchase);
		 */
		
		return $purchase;
	}
}