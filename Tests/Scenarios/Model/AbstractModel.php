<?php

namespace PurchaseBundle\Tests\Scenarios\Model;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractModel
 *
 * @author David Wolter <david@lovoo.com>
 */
abstract class AbstractModel
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    protected function saveObject($object)
	{
		$this->em->persist($object);
		$this->em->flush();
	}
}