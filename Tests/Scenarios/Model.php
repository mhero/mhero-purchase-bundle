<?php

namespace PurchaseBundle\Tests\Scenarios;

use Symfony\Bundle\FrameworkBundle\Client;
use PurchaseBundle\Tests\Scenarios\Model\PurchaseModel;
use PurchaseBundle\Tests\Scenarios\Model\UserModel;

/**
 * Class Model
 *
 * @author David Wolter <david@lovoo.com>
 */
class Model
{
    /**
     * @var UserModel
     */
    public $user;
	/**
	 * @var PurchaseModel
	 */
	public $purchase;
    
    /**
     * Model constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        $this->user = new UserModel($em);
		$this->purchase = new PurchaseModel($em);
		$this->transaction = new TransactionModel($em);
    }
}