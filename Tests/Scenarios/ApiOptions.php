<?php

namespace PurchaseBundle\Tests\Scenarios;

use ApiBundle\Request\ApiRequest;
use Miner\User\Model\UserInterface;

class ApiOptions
{
    const NO_RANDOM = 1;
    const REQUEST = 2; //do not check for a successfuly request
    const HYDRATE = 4;

    /**
     * @var array
     */
    public $parameter;
    /**
     * @var array
     */
    public $server;
    /**
     * @var UserInterface|null
     */
    public $user;
    /**
     * @var UserInterface|null
     */
    public $secondUser;
    /**
     * @var int
     */
    private $options;

    /**
     * @var array
     */
    private $route;

    /**
     * @var string
     */
    private $hydrator;

    /**
     * ApiOptions constructor.
     *
     * @param array $parameter
     * @param int   $options
     */
    public function __construct(array $parameter = array(), $options = 0)
    {
        $this->parameter = $parameter;
        $this->options = $options;
    }

    /**
     * @param array $args
     *
     * @return ApiOptions
     */
    public static function buildOptions(array $args)
    {
        $parameter = [];
        $user = $secondUser = null;
        $options = 0;

        foreach ($args as $arg) {
            if ($arg instanceof UserInterface) {
                if (null === $user) {
                    $user = $arg;
                } else {
                    $secondUser = $arg;
                }           
            } elseif (is_array($arg)) {
                $parameter = array_merge($parameter, $arg);
            } elseif (preg_match('/^\d+\.\d+\.\d+$/', $arg)) {
                $parameter['HTTP_'. ApiRequest::HEADER_API_VERSION] = $arg;
            } elseif (is_int($arg) && $arg > 0) {
                $options |= $arg;
            }
        }

        $server = [];
        foreach ($parameter as $k => $v) {
            if (preg_match('/^HTTP_/', $k)) {
                $server[$k] = $v;
                unset($parameter[$k]);
            }
        }

        $apiOptions = new ApiOptions($parameter, $options);
        $apiOptions->server = $server;
        $apiOptions->user = $user;
        $apiOptions->secondUser = $secondUser;

        return $apiOptions;
    }

    /**
     * @param array $parameter
     */
    public function addParameter($parameter)
    {
        foreach ($parameter as $key => $value) {
            if (!isset($this->parameter[$key])) {
                $this->parameter[$key] = $parameter[$key];
            }
        }
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setRoute($method, $route)
    {
        $this->route = [$method, $route];
    }

    public function isRandom()
    {
        return !(($this->options & ApiOptions::NO_RANDOM) !== 0);
    }

    public function isSuccess()
    {
        return !(($this->options & ApiOptions::REQUEST) !== 0);
    }

    public function mustHydrate()
    {
        return ($this->options & ApiOptions::HYDRATE) !== 0;
    }

    /**
     * @return string
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * @param string $hydrator
     */
    public function setHydrator($hydrator)
    {
        $this->hydrator = $hydrator;
    }
}