<?php

namespace PurchaseBundle\Tests\Scenarios;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miner\User\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ApiHydrater
 *
 * @author David Wolter <david@lovoo.com>
 */
class ApiHydrater
{
    const USER = 'user';

    /**
     * @var Registry
     */
    public $doctrine;

    /**
     * Api constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->doctrine = $client->getContainer()->get('doctrine');
    }

    /**
     * @param string $type
     * @param object $data
     *
     * @return mixed
     */
    public function hydrate($type, $data)
    {
        switch ($type) {
            case ApiHydrater::USER:
                return $this->user($data->data->id);
        }

        throw new \RuntimeException('unknown hydrator');
    }

    /**
     * @param $object
     */
    public function detach($object)
    {
        if (!is_array($object)) {
            $object = array($object);
        }
        foreach ($object as $fe) {
            $this->doctrine->getManager()->detach($fe);
        }
    }

    /**
     * @param $object
     */
    public function refresh($object)
    {
        if (!is_array($object)) {
            $object = array($object);
        }
        foreach ($object as $fe) {
            $this->doctrine->getManager()->refresh($fe);
        }
    }

    /**
     * @param int $id
     *
     * @return UserInterface
     */
    public function user($id)
    {
        return $this->doctrine->getRepository('LovoonowBundle:User')->find($id);
    }
}