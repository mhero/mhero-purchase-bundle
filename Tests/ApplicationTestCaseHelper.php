<?php

namespace PurchaseBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * @author David Wolter <david@lovoo.com>
 */
class ApplicationTestCaseHelper
{
    /**
     * @var Application
     */
    protected $application;
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var SqliteTestCaseHelper
     */
    public $sqlite;

    /**
     * @param Kernel $kernel
     */
    public function __construct(Kernel $kernel)
    {
        $this->application = new Application($kernel);
        $this->sqlite = new SqliteTestCaseHelper($this->container = $kernel->getContainer());
    }

    /**
     * @param ContainerAwareCommand $command
     *
     * @return string
     */
    public function testCommand(ContainerAwareCommand $command) {

        $this->application->add($command);
        $command->setContainer($this->container);

        $commandTester = new CommandTester($command);
        $commandTester->execute(array('command' => $command->getName()));

        return $commandTester->getDisplay();
    }

    /**
     * @param string $commandLine
     */
    public function console($commandLine, OutputInterface $output = null)
    {
        if (null === $application = $this->application) {
            throw new \RuntimeException('need to "self::createApplication(self::$kernel)" in your setUp()');
        }

        if (null === $output) {
            $output = new NullOutput();
        }

        $application->doRun(new StringInput($commandLine), $output);
    }

    public function dropDatabase()
    {
        $this->sqlite->useOrCreateSqliteCache(
            function () {
                $this->console('doctrine:schema:drop --force');
                $this->console('doctrine:schema:create');
            }
        );
    }
}