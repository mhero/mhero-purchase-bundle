<?php

namespace PurchaseBundle\Tests;

use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * @author David Wolter <david@lovoo.com>
 */
class SqliteTestCaseHelper
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param Kernel $kernel
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param callable    $closureToCreate
     * @param callable    $closureToUse
     * @param string|null $name
     */
    public function useOrCreateSqliteCache(\Closure $closureToCreate, \Closure $closureToUse=null, $name = null)
    {
        if (true === $this->useSqliteCache($name)) {
            if(null !== $closureToUse) {
                $closureToUse();
            }

            return;
        }

        $closureToCreate();

        $this->createSqliteCache($name);
    }

    /**
     * @param string|null $name
     *
     * @return array|null
     */
    private function useSqliteCache($name = null)
    {
        $pathSqlite = $this->getSqlitePath();
        $pathDist = $this->createDistPath($pathSqlite, $name);

        if (null === $pathSqlite || !is_file($pathDist)) {
            return null;
        }

        if (!copy($pathDist, $pathSqlite)) {
            throw new \RuntimeException('copy sql error');
        }

        return true;
    }

    /**
     * @param string|null $name
     *
     * @return array|null
     */
    private function createSqliteCache($name = null)
    {
        if (null === $pathOrigin = $this->getSqlitePath()) {
            return null;
        }

        if (!copy($pathOrigin, $this->createDistPath($pathOrigin, $name))) {
            throw new \RuntimeException('error while creating sql dist');
        }
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    private function getConnection()
    {
        return $this->container->get('doctrine.dbal.default_connection');
    }

    /**
     * @param string|null $name
     *
     * @return string|null
     */
    private function getSqlitePath($name = null)
    {
        $params = $this->getConnection()->getParams();

        if ('pdo_sqlite' !== $params['driver']) {
            return null;
        }

        $path = $params['path'];

        if (!empty($name)) {
            $path = $this->createDistPath($path, $name);
        }

        return $path;
    }

    /**
     * @param string      $pathOrigin
     * @param string|null $name
     *
     * @return string
     */
    private function createDistPath($pathOrigin, $name = null)
    {
        if (!empty($name)) {
            $name = '.'.$name;
        }

        return $pathOrigin.$name.'.dist';
    }
}