<?php

namespace PurchaseBundle\Tests;

use ApiBundle\Enum\ApiVersions;
use ApiBundle\Request\ApiRequest;
use Symfony\Component\HttpFoundation\Request;
use PurchaseBundle\Tests\ApplicationTestCaseHelper;
use PurchaseBundle\Tests\Scenarios\Api;
use PurchaseBundle\Tests\Scenarios\ApiHydrater;
use PurchaseBundle\Tests\Scenarios\Model;
use PurchaseBundle\Tests\Scenarios\Scenarios;
use Miner\User\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AbstractClientTestCase
 *
 * @author David Wolter <david@lovoo.com>
 */
abstract class AbstractClientTestCase extends WebTestCase
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Scenarios
     */
    protected $scenarios;

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var ApiHydrater
     */
    public $hydrator;

    protected function initScenarios()
    {
        $this->scenarios = new Scenarios($this->client);
        $this->api = $this->scenarios->api;
        $this->model = $this->scenarios->model;
        $this->hydrator = $this->scenarios->hydrator;
    }

    /**
     * @param               $method
     * @param               $uri
     * @param array         $parameters
     * @param UserInterface $user
     * @param array         $server
     *
     * @return object
     */
    protected function doSuccessfulRequest(
    $method, $uri, array $parameters = array(), UserInterface $user = null, array $server = array()
    )
    {
        $content = $this->request($method, $uri, $parameters, $user, $server);

        self::assertResponseSuccessful();

        return $content;
    }

    /**
     * @param               $method
     * @param               $uri
     * @param array         $parameters
     * @param UserInterface $user
     * @param array         $server
     *
     * @return object
     */
    protected function request(
    $method, $uri, array $parameters = array(), UserInterface $user = null, array $server = array()
    )
    {
        $server['HTTP_accept'] = 'application/json';

        if (!isset($server['HTTP_' . ApiRequest::HEADER_API_VERSION])) {
            $server['HTTP_' . ApiRequest::HEADER_API_VERSION] = ApiVersions::CURRENT;
        }
        if (!isset($server['HTTP_' . ApiRequest::HEADER_PLATFORM])) {
            $server['HTTP_' . ApiRequest::HEADER_PLATFORM] = ApiRequest::PLATFORM_IOS;
        }

        if (null !== $user) {
            $server['HTTP_' . ApiRequest::HEADER_AUTH_KEY] = sprintf(
                    'auth-%s-%s', $user->getId(), $user->getPassword()
            );
        }

        $serverOrigin = $_SERVER;
        $_SERVER = array_merge($_SERVER, $server);

        $requestParams = [];
        foreach ($parameters as $key => $value) {
            $requestParams[] = implode("=", [$key, $value]);
        }

        $this->client->request($method, $uri, $parameters, array(), $server, implode("&", $requestParams));

        $_SERVER = $serverOrigin;

        return json_decode($this->client->getResponse()->getContent());
    }

    /**
     * @param int $code
     */
    public function assertResponseSuccessful($code = null)
    {
        $codeReturned = $this->client->getResponse()->getStatusCode();
        $codeReturnedString = (string) $codeReturned;

        $message = '[Request]' . PHP_EOL . $this->client->getRequest()->getRequestUri() . PHP_EOL .
                '[Response]' . PHP_EOL . $this->client->getResponse()->getContent() . PHP_EOL;

        if (null === $code) {
            self::assertTrue(2 === (int) $codeReturnedString[0], $message);
        } else {
            self::assertEquals($code, $codeReturned, $message);
        }
    }
	
	protected function setUp()
	{
		$this->client = static::createClient();
		$this->initScenarios();
		$application = new ApplicationTestCaseHelper($this->client->getKernel());
		$application->dropDatabase();
		
		$this->em = $this->client->getContainer()->get('doctrine.orm.default_entity_manager');
	}
}
