<?php
namespace PurchaseBundle\Controller;

use ApiBundle\Request\ApiRequest;
use PurchaseBundle\Enum\Platform;
use PurchaseBundle\Enum\PurchaseType;
use PurchaseBundle\Event\Events;
use PurchaseBundle\Event\PurchaseResultEvent;
use PurchaseBundle\Model\PurchaseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseController extends Controller
{
	public function verifyPurchaseAction($platform, Request $request)
	{
		$logger = $this->get("logger");
		$verifier = $this->get("purchase.verifier.container");
		$data = [];
		$productId = $request->get("product_id", "-1");
		$data["product_id"] = $productId;
		$purchaseType = $request->get("purchaseType", PurchaseType::CONSUMABLE);
		$data["purchaseType"] = $purchaseType;
		$transactionId = $request->get("transaction_id", "invalid");
		$data["transaction_id"] = $transactionId;
		$requestReceiptValue = str_replace("\\", "", base64_decode($request->get("receipt", "invalid")));
		$logger->err(sprintf("[Purchase Verifier] %s, decoded: %s", $requestReceiptValue, json_decode($requestReceiptValue, true)));
		
		$isDev = false;
		$originalJson = [];
		$iosReceipt = [];
		switch ($platform) {
			case Platform::ANDROID:
				$data["package_name"] = $this->getParameter("package_name");
				$data["purchase_token"] = $request->get("purchase_token", "invalid");
				$data["receipt"] = $request->get("receipt", "invalid");
				$originalJson = json_decode($data["receipt"], true)["Payload"];
				break;
			case Platform::IOS:
				// use Payload for receipt validation as it keeps the base64 data
				$iosReceipt = json_decode($requestReceiptValue, true);
				$data["receipt"] = $iosReceipt["Payload"];
				$isDev = ($request->get("isDev", -1) > 0);
				break;
		}
		$user = $this->getUser();
		
		if ($this->getParameter("pb.save.active") && ApiRequest::isMinAppVersion($this->getParameter("pb.save.minappversion"))) {
			$repo = $this->getRepository();
			$userClass = $this->getParameter("pb.user.class");
			$logger->error("User: " . (new $userClass())->class);
			// ToDo: change to make it more variable | custom getUser()
			if (null !== $purchase = $repo->findByReference($data["transaction_id"])) {
				$isVerified = $purchase->getVerify();
			} else {
				$additionalData = [];
				$isVerified = $verifier->verify($platform, $data, $isDev);
				$purchase = $repo->createPurchase();
				if (isset($data["package_name"])) {
					$purchase->setPackageName($data["package_name"]);
				}
				if (isset($data["product_id"])) {
					$additionalData["product"] = $productId;
					$purchase->setProduct($productId);
				}
				if (isset($data["purchase_token"])) {
					$purchase->setToken($data["purchase_token"]);
				}
				if (isset($data["receipt"])) {
					$logger->err(sprintf("[Purchase Verifier] data: %s", $data["receipt"]));
					if ($platform == Platform::IOS) {
						$receipt = $iosReceipt;
					} else {
						$receipt = str_replace("\\", "", base64_decode($data["receipt"]));
					}
					$additionalData["receipt"] = $receipt;
					$logger->err(sprintf("[Purchase Verifier] receipt %s", $receipt));
//					if ($platform == Platform::IOS) {
//						$purchase->setToken($data["receipt"]);
//					}
				}
				
				$logger->err(sprintf("[Purchase Verifier] original json %s", $originalJson));
				if (null !== $originalJson && false !== $originalJson) {
					$purchase->setOriginalJson($originalJson);
				}
				
				$purchase->setReference($data["transaction_id"]);
				$purchase->setVerify($isVerified);
				$purchase->setType($data["purchaseType"]);
				$purchase->setState("finished");
				$logger->err(sprintf("[Purchase Verifier] additional data %s", $additionalData));
				if (false !== $additionalData) {
					$purchase->setAdditionalData($additionalData);
				}
				$purchase->setUser($user);
				$purchase->setCreationDate(new \DateTime());
				try {
					$repo->savePurchase($purchase);
				} catch (\PDOException $exception) {
					$isVerified = false;
				}
			}
		} else {
			$isVerified = $verifier->verify($platform, $data, $isDev);
		}
		
		$purchaseResultEvent = new PurchaseResultEvent();
		$purchaseResultEvent->setUserId($user->getId());
		$purchaseResultEvent->setProductId($productId);
		$purchaseResultEvent->setPurchaseType($purchaseType);
		$purchaseResultEvent->setPlatform($platform);
		$purchaseResultEvent->setAppVersion(ApiRequest::$appVersion);
		
		$eventDispatcher = $this->get("event_dispatcher");
		$logger->error($isVerified ? "[Purchase Verifier] is verified" : "[Purchase] is not verified");
		if ($isVerified) {
			$eventDispatcher->dispatch(Events::SUCCESSFUL_PURCHASE, $purchaseResultEvent);
			return new Response(json_encode([
				"data" => ["verified" => "success", "product_id" => $productId,
						   "type"     => $data["purchaseType"]]
			]));
		} else {
			$eventDispatcher->dispatch(Events::FAILED_PURCHASE, $purchaseResultEvent);
		}
		
		return new Response(json_encode([
			"data" => ["verified" => "failed", "product_id" => $productId,
					   "type"     => $data["purchaseType"]]
		]));
	}
	
	public function changePurchaseStatusAction(Request $request)
	{
		$transactionId = $request->get("transaction_id", "invalid");
		if (!$this->getParameter("pb.save.active") || !ApiRequest::isMinAppVersion($this->getParameter("pb.save.minappversion"))) {
			return new Response(["msg" => "not available"]);
		}
		
		$user = $this->getUser();
		/** @var PurchaseInterface $purchase */
		if (null !== $purchase = $this->getRepository()->findByReference($transactionId)) {
			if ($user->getId() === $purchase->getUser()->getId()) {
				$state = $request->get("state", "undefined");
				if ($state !== "undefined" && $state != $purchase->getState()) {
					$purchase->setState($state);
					$purchase->setUpdatedAt(new \DateTime());
					$this->getRepository()->savePurchase($purchase);
				}
			}
		}
		
		return new Response(["msg" => "updated"]);
	}
	
	protected function getRepository()
	{
		return $this->getDoctrine()->getRepository("PurchaseBundle:Purchase");
	}
}