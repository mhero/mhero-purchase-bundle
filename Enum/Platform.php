<?php
namespace PurchaseBundle\Enum;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class Platform
{
    const ANDROID       = "android";
    const ANDROID_C     = "Android";
    const IOS           = "ios";
    const IOS_C         = "Ios";
    const IPHONE        = "iphone";
    const IPHONE_PLAYER = "iphoneplayer";
    const IPOD          = "ipod";
    const IPAD          = "ipad";

    const TABLET     = "tablet";
    const SMARTPHONE = "smartphone";

    const UNKNOWN   = "unknown";
    const UNDEFINED = "undefined";
}
 