<?php

namespace PurchaseBundle\Enum;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseType
{
	const CONSUMABLE     = "Consumable";
	const NON_CONSUMABLE = "NonConsumable";
	const SUBSCRIPTION   = "Subscription";
}