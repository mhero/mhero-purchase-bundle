<?php
namespace PurchaseBundle\Helper;
use Doctrine\Bundle\DoctrineBundle\Registry;
use PurchaseBundle\Model\PurchaseManagerInterface;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class InAppPackageHelper 
{
    const COINS = "coins";
    const KARMA = "karma";

    /**
     * @var PurchaseManagerInterface
     */
    protected $purchaseManager;
    /**
     * @var array
     */
    protected $packages;


    public function __construct(Registry $registry, array $packages)
    {
    	$this->purchaseManager = $registry->getEntityManager()->getRepository("PurchaseBundle:Purchase");
    	$this->packages = $packages;
    }

    /**
     * Checks if the package exist.
     *
     * @param string $packName
     * @param string $type
     *
     * @return bool
     */
    public function checkIfPackageExist($packName, $type)
    {
    	// todo: rework with config
        if (isset($this->packages[$type])) {
			return in_array($packName, $this->packages[$type]);
        }

        return false;
    }

    /**
     * Check user data and signature.
     * @deprecated replaced by verifier
     */
    public function validatePurchase()
    {
    }
}
