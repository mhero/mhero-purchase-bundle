<?php
namespace PurchaseBundle\Event;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
final class Events
{
	const SUCCESSFUL_PURCHASE = "purchase.successful";
	const FAILED_PURCHASE = "purchase.failed";
}