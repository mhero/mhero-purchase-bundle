<?php
namespace PurchaseBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseResultEvent extends Event
{
	/**
	 * @var int
	 */
	protected $userId;
	/**
	 * @var string
	 */
	protected $productId;
	/**
	 * @var string
	 */
	protected $purchaseType;
	/**
	 * @var string
	 */
	protected $platform;
	/**
	 * @var string
	 */
	protected $appVersion;
	
	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}
	
	/**
	 * @param int $userId
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}
	
	/**
	 * @return string
	 */
	public function getProductId()
	{
		return $this->productId;
	}
	
	/**
	 * @param string $productId
	 */
	public function setProductId($productId)
	{
		$this->productId = $productId;
	}
	
	/**
	 * @return string
	 */
	public function getPurchaseType()
	{
		return $this->purchaseType;
	}
	
	/**
	 * @param string $purchaseType
	 */
	public function setPurchaseType($purchaseType)
	{
		$this->purchaseType = $purchaseType;
	}
	
	/**
	 * @return string
	 */
	public function getPlatform()
	{
		return $this->platform;
	}
	
	/**
	 * @param string $platform
	 */
	public function setPlatform($platform)
	{
		$this->platform = $platform;
	}
	
	/**
	 * @return string
	 */
	public function getAppVersion() : string
	{
		return $this->appVersion;
	}
	
	/**
	 * @param string $appVersion
	 */
	public function setAppVersion(string $appVersion) : void
	{
		$this->appVersion = $appVersion;
	}
}