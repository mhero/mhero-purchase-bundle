<?php
namespace PurchaseBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PurchaseBundle\Entity\Purchase;
use PurchaseBundle\Model\PurchaseInterface;

use DateTime;
use PurchaseBundle\Model\PurchaseUserInterface;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseRepository extends EntityRepository
{
	public function findAllPurchases()
	{
		return $this->findAll();
	}
	
	public function findAllUnverifiedPurchases()
	{
		$qb = $this->createQueryBuilder("p");
		
		$qb->where("p.verify = :verified")
			->andWhere("p.creationDate > :targetDate");
		
		$qb->setParameters([
			"verified" => false,
			"targetDate" => new DateTime("2019-06-01 00:00:00")
		]);
		
		return $qb->getQuery()->getResult();
	}
	
	public function findPurchasesByCreation(DateTime $date)
	{
		return $this->findBy(["creationDate" => $date]);
	}
	
	public function findPurchasesByLastEdit(DateTime $date)
	{
		return $this->findBy(["updatedAt" => $date]);
	}
	
	public function findPurchasesByUser(PurchaseUserInterface $user)
	{
		return $this->findBy(["user" => $user]);
	}
	
	public function findByReference($reference)
	{
		return $this->findOneBy(["reference" => $reference]);
	}
	
	/**
	 * @param PurchaseUserInterface $user
	 * @param string                $token
	 *
	 * @return PurchaseInterface
	 */
	public function findPurchaseByUserAndToken(PurchaseUserInterface $user, $token)
	{
		return $this->findOneBy(["user" => $user, "token" => $token]);
	}
	
	public function createPurchase()
	{
		return new Purchase();
	}
	
	public function savePurchase(PurchaseInterface $purchase)
	{
		$em = $this->getEntityManager();
		$em->persist($purchase);
		$em->flush();
	}
	
	public function deletePurchase(PurchaseInterface $purchase)
	{
		$em = $this->getEntityManager();
		$em->remove($purchase);
		$em->flush();
	}
}