<?php
namespace PurchaseBundle\Repository;

use PurchaseBundle\Model\PurchaseLogInterface;
use PurchaseBundle\Model\PurchaseLogManagerInterface;
use Doctrine\ORM\EntityRepository;
use DateTime;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseLogRepository extends EntityRepository
{
	public function findLogById($id)
	{
		return $this->find($id);
	}
	
	public function findAllLogs()
	{
		return $this->findAll();
	}
	
	public function findLogsByDate(DateTime $start, DateTime $end = null)
	{
		$qb = $this->createQueryBuilder("pl");
		$qb->where($qb->expr()->gte("pl.creation_date", $start));
		$qb->andWhere($qb->expr()->lte("pl.creation_date", $end));
	}
	
	public function createPurchaseLog()
	{
		// TODO: Implement createPurchaseLog() method.
	}
	
	public function savePurchaseLog(PurchaseLogInterface $purchaseLog)
	{
		// TODO: Implement savePurchaseLog() method.
	}
	
	public function deletePurchaseLog(PurchaseLogInterface $purchaseLog)
	{
		// TODO: Implement deletePurchaseLog() method.
	}	
}