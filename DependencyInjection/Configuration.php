<?php
namespace PurchaseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder()
	{
		$builder = new TreeBuilder();
		$rootNode = $builder->root("purchase");
		
		$rootNode
			->children()
				->arrayNode("user")
					->children()
						->scalarNode("class")->defaultValue("")->end()
					->end()
				->end()
				->arrayNode("google")
					->children()
						->scalarNode("validator")->isRequired()->end()
						->scalarNode("package_name")->isRequired()->end()
						->scalarNode("json_file")->isRequired()->end()
					->end()
				->end()
				->arrayNode("save")
					->children()
						->scalarNode("minappversion")->defaultValue("1.0.0")->end()
						->scalarNode("active")->defaultValue(false)->end()
					->end()
				->end()
			->end();
		
		return $builder;
	}
}