<?php
namespace PurchaseBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseExtension extends Extension
{
	public function load(array $configs, ContainerBuilder $container)
	{
		$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);
		
		$loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
		$loader->load('services.yml');
		
		$container->setParameter("pb.user.class", $config["user"]["class"]);
		$container->setParameter("pb.google.validator", $config["google"]["validator"]);
		$container->setParameter("pb.google.package", $config["google"]["package_name"]);
		$container->setParameter("pb.google.json", $config["google"]["json_file"]);
		$container->setParameter("pb.save.active", $config["save"]["active"]);
		$container->setParameter("pb.save.minappversion", $config["save"]["minappversion"]);
	}
	
	/**
	 * Returns the base path for the XSD files.
	 *
	 * @return string The XSD base path
	 */
	public function getXsdValidationBasePath()
	{
		return null;
	}
	
	public function getNamespace()
	{
		return 'purchase';
	}
	
	public function getAlias()
	{
		return 'purchase';
	}
}