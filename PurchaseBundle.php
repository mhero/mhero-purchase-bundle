<?php
namespace PurchaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Sven Markert <sven.markert@mhero.de>
 */
class PurchaseBundle extends Bundle
{
	
}