<?php
namespace PurchaseBundle\Verifier;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class VerifierContainer implements VerifierInterface
{
    /**
     * @var array|VerifierInterface[]
     */
    protected $verifier = [];

    /**
     * {@inheritDoc}
     */
    public function verify($type, array $data, $isDev = false)
    {
        if (!isset($this->verifier[$type]) || empty($data)) {
            return false;
        }

        return $this->verifier[$type]->verify($type, $data, $isDev = false);
    }

    /**
     * Sets the verifier.
     *
     * @param string            $type
     * @param VerifierInterface $verifier
     */
    public function setVerifier($type, VerifierInterface $verifier)
    {
        if (!isset($this->verifier[$type])) {
            $this->verifier[$type] = $verifier;
        }
    }
}
