<?php
namespace PurchaseBundle\Verifier;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
interface VerifierInterface
{
    /**
     * Verifies a purchase.
     *
     * @param string $type
     * @param array  $data
     * @param bool   $isDev
     *
     * @return bool
     */
    function verify($type, array $data, $isDev = false);
}
