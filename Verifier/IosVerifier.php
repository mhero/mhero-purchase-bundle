<?php

namespace PurchaseBundle\Verifier;

use Exception;
use Monolog\Logger;
use ReceiptValidator\iTunes\Validator as iTunesValidator;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class IosVerifier implements VerifierInterface
{
	/**
	 * @var Logger
	 */
	protected $logger;
	
	
	public function __construct(Logger $logger)
	{
		$this->logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function verify($type, array $data, $isSandbox = false)
	{
//        if ($isSandbox) {
//            $verification_uri = 'https://sandbox.itunes.apple.com/verifyReceipt';
//        } else {
//            $verification_uri = 'https://buy.itunes.apple.com/verifyReceipt';
//        }
//        if (isset($data['checksum']) && isset($data["receipt"])) {
//            $checksum = $data['checksum'];
//            $base64EncodedReceipt = $data["receipt"];
//        } else {
//            return false;
////            return '{"result":-1002,"error":"Checksum validation failed"}';
//        }
//
//        $postData = json_encode(array('receipt-data' => $base64EncodedReceipt));
//
//        $ch = curl_init($verification_uri);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
//
//        $response = curl_exec($ch);
//        $errno    = curl_errno($ch);
//        $errmsg   = curl_error($ch);
//        curl_close($ch);
//
//        if ($errno != 0) {
//            throw new Exception($errmsg, $errno);
//        }
//
//        $this->logger->info("[iTunes-Response] ". $response);
//        $responseData = json_decode($response);
//
//        if (!is_object($responseData)) {
//            throw new Exception('Invalid verification response');
//        }
//
//        if (!isset($responseData->status)) {
//            throw new Exception('Invalid verification response');
//        }
//
//        if ($responseData->status != 0) {
//            if ($responseData->status == 21007 && !$isSandbox) {
//                return $this->verify($type, $data, true);
//            } else {
//                throw new Exception('Invalid receipt');
//            }
//        }
//
//        $receipt_info = [
//            'quantity'          =>  $responseData->receipt->quantity,
//            'product_id'        =>  $responseData->receipt->product_id,
//            'transaction_id'    =>  $responseData->receipt->transaction_id,
//            'purchase_date'     =>  $responseData->receipt->purchase_date,
//            'purchase_date_ms'  =>  $responseData->receipt->purchase_date_ms,
//            'app_item_id'       =>  $responseData->receipt->item_id,
//            'bid'               =>  $responseData->receipt->bid,
//            'bvrs'              =>  $responseData->receipt->bvrs
//        ];
//
//        try {
//            $purchase_date_ms = $receipt_info['purchase_date_ms'];
//            $purchase_date_s = 0;
//            if (isset($purchase_date_ms[0]) && strlen($purchase_date_ms) > 3) {
//                $purchase_date_s = substr($purchase_date_ms, 0, strlen($purchase_date_ms) - 3);
//            }
//
//            $computed_checksum = sha1($receipt_info['product_id'] . $receipt_info['quantity'] . $receipt_info['transaction_id'] . $purchase_date_s);
//
//            if ($checksum === $computed_checksum) {
//                return true;
////                return '{"result":0}';
//            } else {
//                return false;
////                return '{"result":-1002,"error":"Checksum validation failed"}';
//            }
//        } catch (Exception $e) {
//            return false;
////            return '{"result":-1003,"error":"' . $e->getMessage() . '"}';
//        }
		
		$endpoint = iTunesValidator::ENDPOINT_PRODUCTION;
		if ($isSandbox) {
			$endpoint = iTunesValidator::ENDPOINT_SANDBOX;
		}
		$validator = new iTunesValidator($endpoint);
		$this->logger->err(sprintf("Purchase IosVerifier ios: %s", $endpoint));

//		ex. $receiptBase64Data = 'ewoJInNpZ25hdHVyZSIgPSAiQXBNVUJDODZBbHpOaWtWNVl0clpBTWlKUWJLOEVkZVhrNjNrV0JBWHpsQzhkWEd1anE0N1puSVlLb0ZFMW9OL0ZTOGNYbEZmcDlZWHQ5aU1CZEwyNTBsUlJtaU5HYnloaXRyeVlWQVFvcmkzMlc5YVIwVDhML2FZVkJkZlcrT3kvUXlQWkVtb05LeGhudDJXTlNVRG9VaFo4Wis0cFA3MHBlNWtVUWxiZElWaEFBQURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RRRUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlFRjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIzSmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5USXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVIVnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVrRndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NXNWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZMEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1YvcnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRkS1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNqQndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBnRVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERnUVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0JnVUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lCM1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNlVFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIwN2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FRVnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xvSHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5qK2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2JwMGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJwdXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREV5TFRBMExUTXdJREE0T2pBMU9qVTFJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkltOXlhV2RwYm1Gc0xYUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSXhNREF3TURBd01EUTJNVGM0T0RFM0lqc0tDU0ppZG5KeklpQTlJQ0l5TURFeU1EUXlOeUk3Q2draWRISmhibk5oWTNScGIyNHRhV1FpSUQwZ0lqRXdNREF3TURBd05EWXhOemc0TVRjaU93b0pJbkYxWVc1MGFYUjVJaUE5SUNJeElqc0tDU0p2Y21sbmFXNWhiQzF3ZFhKamFHRnpaUzFrWVhSbExXMXpJaUE5SUNJeE16TTFOems0TXpVMU9EWTRJanNLQ1NKd2NtOWtkV04wTFdsa0lpQTlJQ0pqYjIwdWJXbHVaRzF2WW1Gd2NDNWtiM2R1Ykc5aFpDSTdDZ2tpYVhSbGJTMXBaQ0lnUFNBaU5USXhNVEk1T0RFeUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dGFXNWtiVzlpWVhCd0xrMXBibVJOYjJJaU93b0pJbkIxY21Ob1lYTmxMV1JoZEdVdGJYTWlJRDBnSWpFek16VTNPVGd6TlRVNE5qZ2lPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVWlJRDBnSWpJd01USXRNRFF0TXpBZ01UVTZNRFU2TlRVZ1JYUmpMMGROVkNJN0Nna2ljSFZ5WTJoaGMyVXRaR0YwWlMxd2MzUWlJRDBnSWpJd01USXRNRFF0TXpBZ01EZzZNRFU2TlRVZ1FXMWxjbWxqWVM5TWIzTmZRVzVuWld4bGN5STdDZ2tpYjNKcFoybHVZV3d0Y0hWeVkyaGhjMlV0WkdGMFpTSWdQU0FpTWpBeE1pMHdOQzB6TUNBeE5Ub3dOVG8xTlNCRmRHTXZSMDFVSWpzS2ZRPT0iOwoJImVudmlyb25tZW50IiA9ICJTYW5kYm94IjsKCSJwb2QiID0gIjEwMCI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9';
		$receiptBase64Data = $data['receipt'];
		
		$response = null;
		try {
			$response = $validator->setReceiptData($receiptBase64Data)->validate();
		} catch (Exception $e) {
			$this->logger->error('Purchase IosVerifier: got error = ' . $e->getMessage());
		}
		
		if (null !== $response && $response->isValid()) {
			$this->logger->error('Purchase IosVerifier: Receipt is valid. Receipt data = ' . $response->getReceipt());
			
			return $response->isValid();
		} else {
			$this->logger->error('Purchase IosVerifier: Receipt is not valid. Receipt result code = ' . $response->getResultCode());
			
			return false;
		}
	}
	
}
