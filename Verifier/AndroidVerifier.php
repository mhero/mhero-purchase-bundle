<?php

namespace PurchaseBundle\Verifier;

use Exception;
use Guzzle\Http\Client;
use Monolog\Logger;
use ReceiptValidator\GooglePlay\Validator as PlayValidator;
use Google_Service_AndroidPublisher;

/**
 * @author Sven Markert <sven.markert@dampfer.net>
 */
class AndroidVerifier implements VerifierInterface
{
	/**
	 * @var string
	 */
	protected $validatorName;
	/**
	 * @var string
	 */
	protected $jsonFile;
	/**
	 * @var string
	 */
	protected $packageName;
	
	/**
	 * @var Logger
	 */
	protected $logger;
	
	public function __construct($validatorName, $jsonFile, $packageName, Logger $logger)
	{
		$this->validatorName = $validatorName;
		$this->jsonFile = $jsonFile;
		$this->packageName = $packageName;
		$this->logger = $logger;
	}
	
	public function verify($type, array $data, $isDev = false)
	{
		try {
			$client = new \Google_Client();
			$client->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
			$client->setApplicationName($this->validatorName);
			$client->setAuthConfig($this->jsonFile);
			
			$validator = new PlayValidator(new Google_Service_AndroidPublisher($client));
			
			$response = $validator->setPackageName($this->packageName)
				->setProductId($data["product_id"])
				->setPurchaseToken($data["purchase_token"])
				->validate();
		} catch (Exception $e) {
			$this->logger->error("AndroidVerifier: " . $e->getMessage());
		
			return false;
		}
		
		return true;
	}
	
}
